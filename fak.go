/*
Search and replace substrings from hash map.
*/

package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type spk struct {
	position int
	key      string
}

func parseUI(str string) ([]spk, error) {
	s := strings.Split(str, ",")
	var ui []spk
	for _, val := range s {
		res := strings.Split(val, ":")
		ipos, err := strconv.Atoi(res[0])
		if err != nil {
			return nil, fmt.Errorf("parsing %s as int: %v", res[0], err)
		}
		ui = append(ui, spk{
			position: ipos,
			key:      res[1],
		})
	}
	return ui, nil
}

func readl(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

func kvfToMap(kvf string) (map[string]string, error) {
	kv, err := readl(kvf)
	if err != nil {
		return nil, err
	}
	m := make(map[string]string)
	for _, line := range kv {
		s := strings.Fields(line)
		var r = regexp.MustCompile("^" + s[0] + "[[:space:]]+")
		m[s[0]] = r.ReplaceAllString(line, "")
	}
	return m, nil
}

func maxPos(ui []spk) int {
	max := 0
	for _, val := range ui {
		tmp := val.position
		if max < tmp {
			max = tmp
		}
	}
	return max
}

func main() {
	rexp := flag.String("rexp", "", "Regular exppression. For examp. '(foo)(bar)([0-9]+)(key1)'")
	kvf := flag.String("kvf", "", "Path to hash file - whitespace separated KEY VALUE pairs per line")
	pval := flag.String("pval", "", "Comma separated submatchPosition:optionalHashKey values.")
	flag.Parse()

	switch {
	case *rexp == "":
		fmt.Fprintf(os.Stderr, "--rexp '(foo)([0-9]+)(bar)' \n")
		os.Exit(1)
	case *kvf == "":
		fmt.Fprintf(os.Stderr, "--kvf /path/to/my/key/value/hash/file \n")
		os.Exit(1)
	case *pval == "":
		fmt.Fprintf(os.Stderr, "--pval 1:keyA,2:,3,keyC \n")
		os.Exit(1)
	}

	ui, err := parseUI(*pval)
	if err != nil {
		fmt.Fprintf(os.Stderr, "", err)
		os.Exit(1)
	}

	m, err := kvfToMap(*kvf)
	if err != nil {
		fmt.Fprintf(os.Stderr, "", err)
		os.Exit(1)
	}

	var r = regexp.MustCompile(*rexp)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Println(r.ReplaceAllStringFunc(scanner.Text(), func(match string) string {
			parts := r.FindStringSubmatch(match)
			for _, val := range ui {
				switch {
				case len(m[val.key]) != 0:
					//user input treated as hash key to substitute submatch with hash value
					parts[val.position] = m[val.key]
				case len(val.key) != 0:
					//user input treated as literal to substitute submatch.
					parts[val.position] = val.key
				case len(m[parts[val.position]]) != 0:
					//use submatch as hash key to substitute submatch with hash value.
					parts[val.position] = m[parts[val.position]]
				default:
					//delete submatch
					parts[val.position] = ""
				}
			}
			return strings.Join(parts[1:], "")
		}))

	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "reading standard input:", err)
	}
}
