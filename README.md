# fak
Search and replace substrings from hash map feed in from stdin.

Use case:</br>
Motivaton to use this piece of code is to get rid of rather lengthy shell oneliners </br>
while read line;do sed | awk | grep | tr | cut | rev | somePerlThing ..... </br>

Compile: </br>
go build fak.go </br>

Replace 'bar' with key0 value from hash map file: </br>
./fak --rexp '(foo)(bar)' --kvf ./keyValue.csv --pval 2:key0 </br.

Delete 'foo', replace 'bar' explicity from key0 hash value, replace number sequence with string literal, use submatch as hash key 'symbols' and replace it with value: </br>
./fak --rexp '(foo)(bar)([0-9]+)(symbols)' --kvf ./keyValue.csv --pval 1:,2:key0,3:SOMENUMBERS,4: </br>

Bugs: </br>
User input not validated. User can use submatch position outside array - panic </br>


